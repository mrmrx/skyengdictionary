//
//  MeaningDetailView.swift
//  SkyengDict
//
//  Created by Michil Khodulov on 26.07.2020.
//  Copyright © 2020 Skyeng. All rights reserved.
//

import SwiftUI

struct MeaningDetailView: View {

    @ObservedObject var viewModel: MeaningDetailViewModel
    
    init(viewModel: MeaningDetailViewModel) {
        self.viewModel = viewModel
    }

    var body: some View {
        List(content: content)
            .onAppear(perform: viewModel.fetchMeaning)
            .navigationBarTitle(viewModel.title)
            .listStyle(GroupedListStyle())
    }
}

private extension MeaningDetailView {

    func content() -> some View {
        if let viewModel = viewModel.dataSource {
            return AnyView(details(for: viewModel))
        } else {
            return AnyView(loading)
        }
    }

    func details(for viewModel: [MeaningDetailRowViewModel]) -> some View {
        MeaningDetailRowView(viewModel: viewModel)
    }

    var loading: some View {
        Text("Loading...")
            .foregroundColor(.gray)
    }
}
