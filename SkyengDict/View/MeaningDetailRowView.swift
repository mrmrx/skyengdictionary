//
//  MeaningDetailRowView.swift
//  SkyengDict
//
//  Created by Michil Khodulov on 28.07.2020.
//  Copyright © 2020 Skyeng. All rights reserved.
//

import SwiftUI

struct MeaningDetailRowView: View {

    var viewModel: [MeaningDetailRowViewModel]

    init(viewModel: [MeaningDetailRowViewModel]) {
        self.viewModel = viewModel
    }

    var body: some View {
        VStack(alignment: .center) {

            AsyncImage(
                url: viewModel.first?.imageUrl ?? "",
                placeholder: Image("placeholder")
                    .resizable()
                    .scaledToFit()
            ).aspectRatio(contentMode: .fit)

            VStack(alignment: .leading) {
                Text("Translation:")
                    .foregroundColor(.gray)
                    .font(.footnote)
                Text("\(viewModel.first?.translation ?? "n/a")")
                    .font(.title)
                Spacer()

                    Text("Transcription:")
                        .foregroundColor(.gray)
                        .font(.footnote)
                Text("[\(viewModel.first?.transcription ?? "n/a")]")
                Spacer()

                Text("Definition:")
                    .foregroundColor(.gray)
                    .font(.footnote)
                Text("\(viewModel.first?.definition ?? "n/a")")
            }
                .padding(8)
        }
    }
}
