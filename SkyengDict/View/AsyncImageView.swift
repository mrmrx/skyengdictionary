//
// Created by Michil Khodulov on 28.07.2020.
// Copyright (c) 2020 Skyeng. All rights reserved.
//

import SwiftUI

struct AsyncImage<Placeholder: View>: View {

    @ObservedObject private var loader: ImageLoader

    private let placeholder: Placeholder?

    init(url: String, placeholder: Placeholder? = nil) {
        loader = ImageLoader(url: url)
        self.placeholder = placeholder
    }

    var body: some View {
        image
            .onAppear(perform: loader.load)
            .onDisappear(perform: loader.cancel)
    }

    private var image: some View {
        Group {
            if loader.image != nil {
                Image(uiImage: loader.image!)
                    .resizable()
            } else {
                placeholder
            }
        }
    }
}