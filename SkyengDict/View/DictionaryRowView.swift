//
// Created by Michil Khodulov on 27.07.2020.
// Copyright (c) 2020 Skyeng. All rights reserved.
//

import SwiftUI

struct DictionaryRowView: View {

    var viewModel: DictionaryRowViewModel
    
    init(viewModel: DictionaryRowViewModel) {
        self.viewModel = viewModel
    }

    var body: some View {
        Text(viewModel.text)
    }
}
