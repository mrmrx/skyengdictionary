//
//  DictionarySearchView.swift
//  SkyengDict
//
//  Created by Michil Khodulov on 26.07.2020.
//  Copyright © 2020 Skyeng. All rights reserved.
//

import SwiftUI

struct DictionarySearchView: View {

    @ObservedObject var viewModel: DictionarySearchViewModel

    @State private var resultPlaceholderText = ""

    init(viewModel: DictionarySearchViewModel) {
        self.viewModel = viewModel
    }

    var body: some View {
        NavigationView {
            List {
                searchField

                if viewModel.dataSource.isEmpty {
                    emptySection
                } else {
                    foundWordsSection
                }
            }
                .listStyle(GroupedListStyle())
                .navigationBarTitle("Skyeng Dictionary️")
        }
            .gesture(DragGesture().onChanged{_ in UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)})
    }

    // MARK: - View components

    var searchField: some View {
        HStack(alignment: .center) {
            TextField("Введите слово для поиска", text: $viewModel.text)
        }
    }

    var foundWordsSection: some View {
        Section {
            ForEach(viewModel.dataSource) { wordRowViewModel in
                NavigationLink(destination: MeaningDetailViewBuilder.makeMeaningDetailView(id: wordRowViewModel.id)) {
                    DictionaryRowView(viewModel: wordRowViewModel)
                }
            }
        }
    }

    var emptySection: some View {
        Section {
            Text(resultPlaceholderText).onAppear {
                if self.viewModel.text.isEmpty {
                    self.resultPlaceholderText = "Начните вводить слово и здесь будут результаты поиска"
                } else {
                    self.resultPlaceholderText = "Слова не найдены"
                }
            }
                .foregroundColor(.gray)
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        let api = SkyengAPI()
        let viewModel = DictionarySearchViewModel(api: api)
        return DictionarySearchView(viewModel: viewModel)
    }
}
