//
// Created by Michil Khodulov on 28.07.2020.
// Copyright (c) 2020 Skyeng. All rights reserved.
//

import SwiftUI
import Combine
import Foundation

class ImageLoader: ObservableObject {

    @Published var image: UIImage?

    private let url: URL
    private var cancellable: AnyCancellable?

    init(url: String) {
        let url = URL(string: "https:" + url)!
        self.url = url
    }

    deinit {
        cancellable?.cancel()
    }

    func load() {
        cancellable = URLSession.shared.dataTaskPublisher(for: url)
            .map { UIImage(data: $0.data) }
            .replaceError(with: nil)
            .receive(on: DispatchQueue.main)
            .assign(to: \.image, on: self)
    }

    func cancel() {
        cancellable?.cancel()
    }
}
