//
// Created by Michil Khodulov on 27.07.2020.
// Copyright (c) 2020 Skyeng. All rights reserved.
//

import Combine
import Foundation

protocol APIProtocol {

    func meaningsFor(text: String) -> AnyPublisher<[SearchResponse.Word], APIError>

    func meaningInfo(id: String) -> AnyPublisher<[MeaningResponse.Meaning], APIError>
}

final class SkyengAPI: APIProtocol {

    func meaningsFor(text: String) -> AnyPublisher<[SearchResponse.Word], APIError> {
        apiRequest(with: makeWordSearchComponents(text))
    }

    func meaningInfo(id: String) -> AnyPublisher<[MeaningResponse.Meaning], APIError> {
        apiRequest(with: makeMeaningComponents(id))
    }

    // MARK: - Generic API request

    private func apiRequest<T>(with components: URLComponents) -> AnyPublisher<T, APIError> where T: Decodable {
        guard let url = components.url else {
            let error = APIError.network(description: "Couldn't create URL")
            return Fail(error: error).eraseToAnyPublisher()
        }
        debugPrint(url.description)
        return URLSession.shared.dataTaskPublisher(for: URLRequest(url: url))
            .mapError { error in
                .network(description: error.localizedDescription)
            }
            .flatMap(maxPublishers: .max(1)) { pair in
                Self.decode(pair.data)
            }
            .eraseToAnyPublisher()
    }
}

// MARK: - URL components

private extension SkyengAPI {

    private struct SkyengURLComponents {
        static let scheme = "https"
        static let host = "dictionary.skyeng.ru"
        static let path = "/api/public/v1"
    }

    func makeWordSearchComponents(_ text: String) -> URLComponents {
        var components = URLComponents()
        components.scheme = SkyengURLComponents.scheme
        components.host = SkyengURLComponents.host
        components.path = SkyengURLComponents.path + "/words/search"

        components.queryItems = [
            URLQueryItem(name: "search", value: text),
            URLQueryItem(name: "pageSize", value: "15"),
        ]

        return components
    }

    func makeMeaningComponents(_ meaningID: String) -> URLComponents {
        var components = URLComponents()
        components.scheme = SkyengURLComponents.scheme
        components.host = SkyengURLComponents.host
        components.path = SkyengURLComponents.path + "/meanings"

        components.queryItems = [
            URLQueryItem(name: "ids", value: meaningID)
        ]

        return components
    }
}

// MARK: - Response Decoder

private extension SkyengAPI {

    static func decode<T: Decodable>(_ data: Data) -> AnyPublisher<T, APIError> {
        let decoder = JSONDecoder()

        let decoded: AnyPublisher<T, APIError> = Just(data)
            .decode(type: T.self, decoder: decoder)
            .mapError { error in
                .parsing(description: error.localizedDescription)
            }
            .eraseToAnyPublisher()
        return decoded
    }
}
