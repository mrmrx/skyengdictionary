//
// Created by Michil Khodulov on 28.07.2020.
// Copyright (c) 2020 Skyeng. All rights reserved.
//

import Foundation

enum APIError: Error {

    case parsing(description: String)
    case network(description: String)
}