//
//  Translation.swift
//  SkyengDict
//
//  Created by Michil Khodulov on 29.07.2020.
//  Copyright © 2020 Skyeng. All rights reserved.
//

import Foundation

struct Translation: Decodable {
    let text: String
}
