//
//  SearchResponse.swift
//  SkyengDict
//
//  Created by Michil Khodulov on 29.07.2020.
//  Copyright © 2020 Skyeng. All rights reserved.
//

import Foundation

enum SearchResponse {

    struct Word: Decodable {

        let id: Int
        let text: String
        let meanings: [Meaning]
    }

    struct Meaning: Decodable {
    
        let id: Int
        let partOfSpeechCode: String
        let translation: Translation
        let transcription: String?
        let imageUrl: String?
    }
}
