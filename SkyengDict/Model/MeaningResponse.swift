//
// Created by Michil Khodulov on 28.07.2020.
// Copyright (c) 2020 Skyeng. All rights reserved.
//

import Foundation

enum MeaningResponse {

    struct Meaning: Decodable {

        let id: String
        let partOfSpeechCode: PartOfSpeechCode
        let text, transcription: String
        let translation: Translation
        let images: [Image]
        let definition: Definition
    }

    struct Definition: Decodable {
        let text: String
    }

    struct Image: Decodable {
        let url: String
    }

    enum PartOfSpeechCode: String, Codable {
        case j = "j"
        case n = "n"
        case ph = "ph"
        case v = "v"
    }
}
