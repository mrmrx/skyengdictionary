//
// Created by Michil Khodulov on 27.07.2020.
// Copyright (c) 2020 Skyeng. All rights reserved.
//

import SwiftUI
import Combine

final class DictionaryRowViewModel: ObservableObject, Identifiable {

    private let word: SearchResponse.Word

    var id: String

    var text: String {
        word.text
    }

    init(word: SearchResponse.Word) {
        self.word = word
        self.id = String(word.meanings.first!.id)
    }
}
