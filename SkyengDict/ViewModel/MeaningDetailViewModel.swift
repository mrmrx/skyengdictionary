//
// Created by Michil Khodulov on 27.07.2020.
// Copyright (c) 2020 Skyeng. All rights reserved.
//

import Foundation
import Combine

final class MeaningDetailViewModel: ObservableObject {

    @Published var dataSource: [MeaningDetailRowViewModel]?

    var title: String {
        if let dataSource = dataSource, let meaningVM = dataSource.first {
            return meaningVM.title
        }
        return "..."
    }

    private var id: String

    private let api: APIProtocol
    private var disposables = Set<AnyCancellable>()

    init(id: String, api: APIProtocol) {
        self.id = id
        self.api = api
    }

    func fetchMeaning() {
        api.meaningInfo(id: id)
            .map { meanings in
                meanings.map(MeaningDetailRowViewModel.init)
            }
            .receive(on: DispatchQueue.main)
            .sink(receiveCompletion: { [weak self] value in
                guard let self = self else { return }

                debugPrint(value)
                switch value {
                case .failure:
                    self.dataSource = []
                case .finished:
                    break
                }
            }, receiveValue: { [weak self] meaning in
                guard let self = self else { return }

                debugPrint(meaning)
                self.dataSource = meaning
            })
            .store(in: &disposables)
    }

}
