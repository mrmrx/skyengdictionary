//
//  MeaningDetailRowViewModel.swift
//  SkyengDict
//
//  Created by Michil Khodulov on 28.07.2020.
//  Copyright © 2020 Skyeng. All rights reserved.
//

import Combine

final class MeaningDetailRowViewModel: ObservableObject {
    
    private let meaning: MeaningResponse.Meaning

    var title: String {
        meaning.text
    }
    var definition: String {
        meaning.definition.text
    }

    var translation: String {
        meaning.translation.text
    }

    var imageUrl: String {
        meaning.images.first!.url
    }

    var transcription: String {
        meaning.transcription
    }

    init(meaning: MeaningResponse.Meaning) {
        self.meaning = meaning
    }
    
}
