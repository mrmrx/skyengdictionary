//
//  DictionarySearchViewModel.swift
//  SkyengDict
//
//  Created by Michil Khodulov on 26.07.2020.
//  Copyright © 2020 Skyeng. All rights reserved.
//

import SwiftUI
import Combine

final class DictionarySearchViewModel: ObservableObject, Identifiable {

    @Published var text: String = ""

    @Published var dataSource: [DictionaryRowViewModel] = []

    private let api: APIProtocol

    private var disposables = Set<AnyCancellable>()

    init(api: APIProtocol, scheduler: DispatchQueue = DispatchQueue(label: "DictionarySearchViewModel")) {
        self.api = api

        $text
            .dropFirst(1)
            .debounce(for: .seconds(0.5), scheduler: scheduler)
            .sink(receiveValue: searchWords(for:))
            .store(in: &disposables)
    }

    func searchWords(for text: String) {
        api.meaningsFor(text: text)
            .map { response in
                response.map(DictionaryRowViewModel.init)
            }
            .receive(on: DispatchQueue.main)
            .sink(
                receiveCompletion: { [weak self] value in
                    guard let self = self else { return }

                    debugPrint(value)
                    switch value {
                    case .failure:
                        self.dataSource = []
                    case .finished:
                        break
                    }
                },
                receiveValue: { [weak self] items in
                    guard let self = self else { return }

                    debugPrint(items)
                    self.dataSource = items
                })

            .store(in: &disposables)
    }
}