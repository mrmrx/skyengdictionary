//
//  MeaningDetailViewBuilder.swift
//  SkyengDict
//
//  Created by Michil Khodulov on 28.07.2020.
//  Copyright © 2020 Skyeng. All rights reserved.
//

import SwiftUI

enum MeaningDetailViewBuilder {
    
    static func makeMeaningDetailView(id: String) -> some View {
        let api = SkyengAPI()
        let viewModel = MeaningDetailViewModel(id: id, api: api)
        let view = MeaningDetailView(viewModel: viewModel)
        return view
    }
}
