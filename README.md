# README #

Реализация англо-русского (и наоборот) словаря.  
Используется API от Skyeng для поиска переводов, значений, картинки и транскрипции.  
Решил использовать SwiftUI & Combine, т.к. хотелось попробовать что-то новое для себя.

![Search screenshot](Screen1.png "Search screen")	 ![Word meaning screenshot](Screen2.png "Word meaning screen")

### Требования для запуска ###
* Xcode 11<
* iOS 13.0<

### Использованные инструменты ###
* Swift 5.2
* Xcode 11.5

### Использованные библиотеки ###

* SwiftUI
* Combine
