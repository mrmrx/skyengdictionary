//
//  SkyengDictTests.swift
//  SkyengDictTests
//
//  Created by Michil Khodulov on 26.07.2020.
//  Copyright © 2020 Skyeng. All rights reserved.
//

import XCTest
@testable import SkyengDict

class SkyengDictTests: XCTestCase {

    var sut: DictionarySearchView!

    override func setUp() {
        super.setUp()

        let api = SkyengAPI()
        let viewModel = DictionarySearchViewModel(api: api)
        sut = DictionarySearchView(viewModel: viewModel)
    }
    
    override func tearDown() {
        sut = nil
        super.tearDown()
    }

    func testSutNotNil() {

        XCTAssertNotNil(sut)
    }
    
    func testDictionarySearchViewBodyNotNil() {
        let value = sut.body
        XCTAssertNotNil(value)
    }

}
